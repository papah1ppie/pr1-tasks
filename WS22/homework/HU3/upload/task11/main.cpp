// Largest power of two to a given number
// Author: Illia Bilyk

#include <iostream>

using std::cout, std::cin;

int main() {
    int n{0}, m{0};
    cout << "Input n: ";
    cin >> n;
    for (int i{n}; i >= 1; i--) {
        if ((i & (i - 1)) == 0) {
            m = i;
            break;
        }
    }
    cout << "Largest power of two that divides the read number: " << m;
    return 0;
}
