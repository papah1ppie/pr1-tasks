// Factorial Loop
// Author: Illia Bilyk

#include <iostream>

using std::cout, std::cin;

unsigned long long int fct(unsigned int n);

int main() {
    unsigned int n{0};
    cout << "Factorial Computing\nEnter n: ";
    cin >> n;
    cout << "n! = " << fct(n);
    return 0;
}

unsigned long long int fct(unsigned int n) {
    unsigned int c{1}; // counter
    for (unsigned int i = 1; i <= n; ++i) {
        c *= i;
    }
    return c;
}
