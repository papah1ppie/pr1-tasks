#include <iostream>
#include <cmath>

using std::cout, std::cin;

int main () {
    double a{0}, b{0}, y{0}, c{0};
    cout << "Seite a Länge: ";
    cin >> a;

    cout << "Seite b Länge: ";
    cin >> b;

    cout << "Grad y: ";
    cin >> y;
    if (!cin || y >= 180) {
        cout << "ERR: kein Zahl oder größer als 180 Grad";
        return 1;
    }
    c = sqrt(a*a + b*b - 2*a*b*cos(y*3.14159/180));
    
    cout << "Seite c Länge: " << c << "\n";

    return 0;
}