#include <iostream>
#include <cmath>

using std::cin, std::cout;

int main() {
    double A{0}, z{0}, E{0}, n{0};

    cout << "Anfangsbetrag: ";
    cin >> A;

    cout << "Zinssatz (in Prozent): ";
    cin >> z;
    if (!cin || z > 100) {
        cout << "ERR: entweder zahl zu groß ist, oder probier noch mal ohne %";
        return 1;
    }

    cout << "gewünschtes Endguthaben: ";
    cin >> E;

    /*  Formel
        n = ( ln(E)-ln(A) )  /  ln(1 + z/100) 
    */

    n = (log(E) - log(A)) / log(1+z/100);
    cout << "Anzahl von Jahren: " << (int) n + 1 << "\n";

    return 0;
}
