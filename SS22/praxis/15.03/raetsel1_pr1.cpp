#include <iostream>
using namespace std;

// extern int abc;
// extern ostream std::cout;

int main() {
    //    cout << abc;

    int i;
    cout << "Geben Sie eine Zahl ein: ";
    cin >> i;

    int j;
    cout << "Geben sie eine Zahl ein: ";
    cin >> j;

    int x{0};
    while (j != 0) {
        if (j % 2 == 1) // Modulo berechnet den Rest der Division
            x = x + i;
        i = i * 2;
        j = j / 2;
    }

    string s{""};
    while (x != 0) {
        if (x % 2 != 0)
            s = '*' + s;
        else
            s = '.' + s;
        x = x / 2;
    }
    cout << s << "\n";

    return 0;
}
