#include <iostream>
using namespace std;

// Dieses Programm ermittelt den durchschnittlichen Benzinverbrauch
// eines Fahrzeuges in l/100km
int main() {
    double km{0};
    cout << "Bitte geben Sie die gefahrenen km ein: ";
    cin >> km;
    if (km == 0) {
        cout << "Fehler! Kann nicht 0 sein!";
        return 127;
    }

    double liter{0};
    cout << "Bitte geben Sie die verbrauchten Liter ein: ";
    cin >> liter;

    cout << "Verbrauch pro 100 km ist " << liter * 100 / km << " L\n";
    return 0;
}
