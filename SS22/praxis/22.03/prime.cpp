#include <cmath>
#include <iostream>

using namespace std;

int main() {
    unsigned int n{0}, div{2};
    cout << "Geben Sie ein Zahl ein: ";
    cin >> n;
    int stop{static_cast<int>(sqrt(n) + 0.5)};
    while (div < stop) {
        if (n % div == 0) {
            cout << n << " ist kein Primzahl!\n";
            return 0;
        }
        ++div;
    }
    cout << n << " ist ein Primzahl!\n";
    return 0;
}
