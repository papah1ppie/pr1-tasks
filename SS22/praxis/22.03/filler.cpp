#include <iostream>

int main() {
    unsigned int i{0};
    while (i < 0xffffffff) {
        std::cout << i << " ";
        ++i;
    }
    return 0;
}
