#include <iostream>
#include <vector>

using namespace std;

int main() {
    int input{0}, min{0};
    cin >> min;
    while (cin >> input) {
        if (input < min) {
            min = input;
        }
    }
    cout << min << "\n";
    return 0;
}
