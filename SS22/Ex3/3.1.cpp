#include <algorithm>
#include <iostream>
#include <vector>

using namespace std;

int main() {
    vector<double> v = {0, 0, 0};
    cout << "Geben Sie drei Zahlen ein (nach jeder Zahl SPACE drücken): ";
    for (auto i = 0; i < static_cast<int>(v.size()); i++)
        cin >> v.at(i);
    sort(v.begin(), v.end());
    for (auto i = 0; i < static_cast<int>(v.size()); i++)
        cout << v.at(i) << " ";
    return 0;
}
