#include <iostream>

using namespace std;

// TODO: Schreiben Sie ein Programm, das zwei natürliche Zahlen m und n einliest
// und die n-te Ziffer der Zahl m ausgibt. Z.B.: m=1358 n=2 Ausgabe: 5 (Die
// Ziffern werden ausgehend vom Dezimalpunkt gezählt).

int main() {
    int m{0}, n{0}, t{0}, i{0};
    cout << "Geben Sie natürliche Zahl m ein: ";
    cin >> m;
    cout << "Geben Sie Numer der Ziffer in Zahl m: ";
    cin >> n;
    if (n > static_cast<int>(to_string(m).length())) {
        cout << "Fehler! Out of bounds!\n";
        return 127;
    }
    for (; m != 0 && i != n; m /= 10) {
        t = m % 10;
        ++i;
    }
    cout << t << '\n';
    return 0;
}
