#include <iostream>

using namespace std;

// TODO: Schreiben Sie ein Programm, das eine dreistellige Zahl einliest und die
// Zahl ausgibt, die durch Umkehrung der Ziffernfolge entsteht.

int main() {
    int n{0};
    cout << "Geben Sie ein Zahl ein: ";
    cin >> n;
    if (to_string(n).length() != 3) {
        cout << "Fehler! Zahl ist größer oder kleiner als erwartet!";
        return 127;
    }
    for (; n != 0; n /= 10)
        cout << n % 10;
    cout << "\n";

    return 0;
}
