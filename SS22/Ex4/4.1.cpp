#include <algorithm>
#include <iostream>

using std::string, std::cout, std::cin, std::remove;

int main() {
    string input{""};
    char remChar{};
    while (cout << "Geben Sie ein Wort ein: " && cin >> input) {
        cout << "Geben Sie ein char ein: ";
        cin >> remChar;
        cout << "Wort " << input;
        input.erase(remove(input.begin(), input.end(), remChar), input.end());
        cout << " ohne Buchstabe " << remChar << ": " << input << '\n';
    }
    return 0;
}
