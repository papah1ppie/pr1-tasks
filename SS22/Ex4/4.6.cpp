#include <iostream>

using std::cin, std::cout;

int gcd(int a, int b) {
    if (b == 0)
        return a;
    return gcd(b, a % b);
}

int main() {
    int a, b;
    cout << "Geben Sie Zähler ein: ";
    cin >> a;
    cout << "Geben Sie Nenner ein: ";
    cin >> b;
    cout << "Ihre Bruch ist " << a << '/' << b << '\n';
    int c{gcd(a, b)};
    cout << "Gekürzte Version Ihres Bruches ist " << a / c << '/' << b / c
         << '\n';
    return 0;
}
