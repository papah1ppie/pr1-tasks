#include <iostream>
#include <vector>

using namespace std;

int main() {
    string input{""};
    while (!cin.eof()) {
        cout << "Geben Sie der Text ein: ";
        getline(cin, input);
        vector<char> line(input.begin(), input.end());
        for (auto i = line.rbegin(); i != line.rend(); i++)
            cout << *i;
        cout << endl;
    }
    return 0;
}
