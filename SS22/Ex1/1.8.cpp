#include<iostream>

using namespace std;

int main() {
    const double c {2.998e8};
    const double lightYear {9.461e12};
    const unsigned short int alphaCentauri {4367};
    // result will be approximate
    double x {0};
    cout << "Geben Sie x Prozent ein (Prozent der Lichtgeschwindigkeit): ";
    cin >> x;
    cout << "Die Zeit, die das Raumschiff braucht, um Alpha Centauri mit gegebenen x Prozent von Lichtgeschwindigkeit zu erreichen ist " << (alphaCentauri * lightYear) / (c * (x / 100)) << " s";
    // Frage
    // Ist const deklination hier nützlich oder braucht man das gar nicht?
    // Wieviel Zeit ist auf der Erde vergangen? - Wie zahlt man das überall?
    // Ist es eine gute Idee, alle diese Zahlen abzukürzen? Um es lesbarer zu machen.
    // Danke im Voraus! Feedback hilft mir sehr!
    return 0;
}
