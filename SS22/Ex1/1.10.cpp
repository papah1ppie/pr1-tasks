#include<iostream>
#include<cmath>

using namespace std;

// TODO: Lesen Sie einen Wert x, sowie die Koeffizienten eines Polynoms vierten Grades ein und berechnen Sie den Wert des Polynoms an der Stelle x. 

int main() {
    // f(x) = a*x^4 + b*x^3 + c*x^2 + d*x + e
    double x {0}, a {0}, b{0}, c{0}, d{0}, e{0}, wert {0};
    
    cout << "Geben Sie x ein: ";
    cin >> x;
    cout << "Geben Sie a Koeffizient ein: ";
    cin >> a;
    if(a==0) {
	cout << "a kann nicht 0 sein!";
	return 137;
    }
    cout << "Geben Sie b Koeffizient ein: ";
    cin >> b;
    cout << "Geben Sie b Koeffizient ein: ";
    cin >> c;
    cout << "Geben Sie d Koeffizient ein: ";
    cin >> d;
    cout << "Geben Sie e Koeffizient ein: ";
    cin >> e;
    
    wert = (a * pow(x, 4)) + (b * pow(x, 3)) + (c * pow(x, 2)) + (d * x) + e;
    cout << "Wert ist " << wert;
    return 0;
}
