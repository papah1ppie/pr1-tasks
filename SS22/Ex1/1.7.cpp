#include<iostream>

using namespace std;

int main() {
    double eingab {0};
    unsigned short int modus {0};
    cout << "Wählen Sie den Konvertierungsmodus, indem Sie die Modusnummer eingeben:\n\t1. USD -> EUR\n\t2. EUR -> USD\nModus: ";
    cin >> modus;
    if(modus == 1) {
	cout << "Geben Sie eine Summe in USD ein: ";
	cin >> eingab;
	cout << eingab << "USD ist " << eingab * 0.92 << " EUR";
    } else if(modus == 2) {
		cout << "Geben Sie eine Summe in EUR ein: ";
		cin >> eingab;
		cout << eingab << "EUR ist " << eingab * 1.09 << " USD";
	} else {
	    cout << "ERR: Es gibt keinen solchen Modus!"; 
	    return 127;
	}
    return 0;
}
